
This is the Viewfinder module. It helps developers find views that may be affected by changing a content type.

Installing: 

Install the module as normal. 
Navigate to administer >> build >> modules. 
Enable Viewfinder.
The menu item should then appear under Admin > Views > Viewfinder.

Contributions: it would be great to have this tool find views based on various other criteria